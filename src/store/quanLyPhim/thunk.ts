import { createAsyncThunk } from '@reduxjs/toolkit'
import { quanLyPhimServices } from 'services'

export const getMovieListThunk = createAsyncThunk(
    'quanLyPhim/getMovieListThunk',
    async (_, { rejectWithValue }) => {
        try {
            const data = await quanLyPhimServices.getMovieList()

            // sleep
            // await new Promise((resolve)=> setTimeout(resolve, 1000))

            return data.data.content
        } catch (err) {
            return rejectWithValue(err)
        }
    }
)
export const getBannerThunk=createAsyncThunk('quanLyPhim/getBannerThunk',async(_,{rejectWithValue})=>{
    try {
      
        const data=await quanLyPhimServices.getBanner()
            return data.data.content

    } catch (err) {
       rejectWithValue(err)
        
    }

})
export const getMovieListPageThunk = createAsyncThunk(
    "quanLyPhim/LayDanhSachPhimPhanTrangThunk",
    async (page: number, { rejectWithValue }) => {
      try {
        const query = `?maNhom=GP08&soTrang=${page}&soPhanTuTrenTrang=10`;
        const data = await quanLyPhimServices.getMovieListPage(query);
        return data.data.content.items;
      } catch (err) {
        return rejectWithValue(err);
      }
    }
  );
  export const getMovieDetailThunk = createAsyncThunk(
    "quanLyPhim/LayThongTinPhimThunk",
    async (payload: string | undefined, { rejectWithValue }) => {
      try {
        const query = `?MaPhim=${payload}`;
        const data = await quanLyPhimServices.getMovieDetail(query);
        return data.data.content;
      } catch (err) {
        return rejectWithValue(err);
      }
    }
  );