// truyền api 

import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyHeThongRapServices } from "services/quanLyRap";

export const getHeThongRapThunk =createAsyncThunk('quanLyRap/getHeThongRapThunk',async(_,{rejectWithValue})=>{
    try {
        const data=await quanLyHeThongRapServices.getHeThongRap()
        return data.data.content
        
    } catch (err) {
        rejectWithValue(err)
    }
}
)
export const getCumRapThunk =createAsyncThunk('quanLyRap/getCumRapThunk', async (payload: string, { rejectWithValue }) => {
    try {
      const query = `?maHeThongRap=${payload}`;
      const data = await quanLyHeThongRapServices.getCumRap(query);
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
}
)
export const getLichChieuThunk = createAsyncThunk(
  "quanLyRap/LayThongTinLichChieuHeThongRapThunk",
  async (payload: string,{ rejectWithValue }) => {
    try {
      const query = `?maHeThongRap=${payload}&maNhom=GP08`;
      console.log("payload",payload)
      const data = await quanLyHeThongRapServices.getLichChieu(query);
// console.log(data)
// const data1=data.data.content[0]
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const getInfoLichChieuPhimThunk = createAsyncThunk(
  "quanLyRap/LayThongTinLichChieuPhim",
  async (payload: string | undefined, { rejectWithValue }) => {
    try {
      const query = `?maPhim=${payload}`;
      const data = await quanLyHeThongRapServices.getInfoLichChieuPhim(query);

      return data.data.content
      
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);