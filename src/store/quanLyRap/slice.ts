import { createSlice } from "@reduxjs/toolkit";
import { QuanLyHeThongRap, CumRap,  LichChieu } from "types";
import {
  getCumRapThunk,
  getHeThongRapThunk,
  getInfoLichChieuPhimThunk,
  getLichChieuThunk,
} from "./thunk";
type QuanLyRapInitialState = {
  heThongRapList: QuanLyHeThongRap[];
  cumRap: CumRap[];
  movies: LichChieu[];
  maLichChieu: number;
};
const initialState: QuanLyRapInitialState = {
  heThongRapList: [],
  cumRap: [],
  movies: [],
  maLichChieu: 41124,
};
const quanLyRapSlice = createSlice({
  name: "quanLyRap",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getHeThongRapThunk.fulfilled, (state, { payload }) => {
        state.heThongRapList = payload;
      })
      .addCase(getCumRapThunk.fulfilled, (state, { payload }) => {
        state.cumRap = payload;
      })
      .addCase(getLichChieuThunk.fulfilled, (state, { payload }) => {
        state.movies = payload;
      })
      .addCase(getInfoLichChieuPhimThunk.fulfilled, (state, { payload }) => {
        // Assuming payload is an array and you want to extract 'maLichChieu' from the first element
        if (payload.length > 0) {
          state.maLichChieu = payload[0].maLichChieu;
        } else {
          state.maLichChieu = null; // Handle the case when the array is empty
        }
      });
  },
});
export const { reducer: quanLyRapReducer, actions: quanLyRapAction } =
  quanLyRapSlice;
