import { apiInstance } from "constant";
import { QuanLyHeThongRap, CumRap, LichChieu } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_RAP_PHIM_API,
});

export const quanLyHeThongRapServices = {
    getHeThongRap:()=>api.get<ApiResponse<QuanLyHeThongRap[]>>('/LayThongTinHeThongRap'),
    getCumRap:(query:string)=>api.get<ApiResponse<CumRap[]>>(`LayThongTinCumRapTheoHeThong${query}`) , 
getLichChieu:(query: string) =>api.get<ApiResponse<LichChieu[]>>(`LayThongTinLichChieuHeThongRap${query}`),
getInfoLichChieuPhim: (query: string) =>
    api.get<ApiResponse<any[]>>(`LayThongTinLichChieuPhim${query}`),
}
