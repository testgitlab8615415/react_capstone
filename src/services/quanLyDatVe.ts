import { apiInstance } from "constant";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_DAT_VE_API,
});
export const quanLyPhongVeService = {
  getDanhSachPhongVe: (query: string) =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    api.get<ApiResponse<any>>(`/LayDanhSachPhongVe${query}`),

};
