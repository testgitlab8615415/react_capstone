import { useSelector } from 'react-redux'
import { RootState } from 'store'

// Lấy thông tin user đăng nhập
export const useAuth = () => {
    const { user } = useSelector((state: RootState) => state.quanLyNguoiDung)
    const { movieList, isFetchingMovieList,banner,currentPage } = useSelector((state: RootState) => state.quanLyPhim)
    const { heThongRapList } = useSelector((state: RootState) => state.quanLyRap)
   
    return { user,currentPage,banner,movieList, isFetchingMovieList,heThongRapList  }

}

