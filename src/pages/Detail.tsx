import { DetailMovieTemplate } from "components/templates";

const Detail = () => {
  return (
    <div>
      <DetailMovieTemplate />
    </div>
  );
};

export default Detail;
