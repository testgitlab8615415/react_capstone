export const Demo = () => {
    return (
        <div>
            <h2>Demo Tailwind</h2>
            <p className="text-20 font-800 text-[#333] py-[20px] hover:text-red-500 transition-all ease-in-out duration-300">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, cum!
            </p>
        </div>
    )
}

export default Demo
