import { useAuth } from "hooks";
import { styled } from "styled-components";

export const Footer = () => {
    const {heThongRapList}=useAuth()
  return (
    <Container className="relative w-full pt-4 pb-4">
      <div className="container mx-auto px-4 mt-[120px]">
            <h2 className="text-center py-7 font-800">CHÚC CÁC BẠN CÓ NHỮNG TRẢI NGHIỆM VUI VẺ</h2>
        <div className="flex flex-wrap text-left lg:text-left">
          <div className="w-full lg:w-6/12 px-4">
            <h4 className="text-3xl fonat-semibold text-blueGray-700">
             Here are some brands
            </h4>
            <h5 className="text-lg mt-0 mb-2 text-blueGray-600">
            We allwayys be by your side
            </h5>
            <div className="mt-6 lg:mb-0 mb-6">
                {heThongRapList.map((logo)=>{
                    return(
              <button key={logo.maHeThongRap}
                className="bg-white text-lightBlue-400 shadow-lg font-normal h-10 w-50 hover:translate-y-[5px] items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
                type="button"
              >
                <img  src={logo.logo} alt="cgv" />
              </button>

                    )
                })
                }
             
            </div>
          </div>
          <div className="w-full lg:w-6/12 px-4">
            <div className="flex flex-wrap items-top mb-6">
              <div className="w-full lg:w-4/12 px-4 ml-auto">
                <span className="block uppercase text-blueGray-500 text-sm font-semibold mb-2">
                  Contact us 19008198 for more
                </span>
                <ul className="list-unstyled">
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="#"
                    >
                      Lịch chiếu
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="#"
                    >
                      Cụm rạp
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="#"
                    >
                      Tin tức
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="#"
                    >
                      Tin tức
                    </a>
                  </li>
                </ul>
              </div>
              <div className="w-full lg:w-4/12 px-4">
                <span className="block uppercase text-blueGray-500 text-sm font-semibold mb-2">
                 privacy & policy 
                </span>
                <ul className="list-unstyled">
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="https://github.com/creativetimofficial/notus-js/blob/main/LICENSE.md?ref=njs-profile"
                    >
                      MIT License
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="https://creative-tim.com/terms?ref=njs-profile"
                    >
                      Terms &amp; Conditions
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="https://creative-tim.com/privacy?ref=njs-profile"
                    >
                      Privacy Policy
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-blueGray-600 hover:text-blueGray-800 font-semibold block pb-2 text-sm"
                      href="https://creative-tim.com/contact-us?ref=njs-profile"
                    >
                      Contact Us
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <hr className="my-5 border-blueGray-300" />
        <div className="w-full md:w-4/12 px-4 mx-auto text-center">
          <div className="text-sm text-blueGray-500 font-semibold py-1 flex items-center justify-center">
            Copyright © <span id="get-current-year">2023</span>
            <p className="text-blueGray-500 hover:text-blueGray-800 pl-2">
              All rights reserved
            </p>
            .
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Footer;
const Container = styled.footer`
  width: var(--max-width);
  margin: auto;
`;
