// import { RiMovie2Fill } from "react-icons/ri";
// import { MdManageAccounts } from "react-icons/md";
// import { RxAvatar } from "react-icons/rx";
// import { useNavigate } from "react-router-dom";
// import styled from "styled-components";

// import { Popover } from ".";
// import { PATH } from "constant";
// import { useAuth } from "hooks";
// import { useAppDispatch } from "store";
// import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
// export const Navbar = () => {
//   const navigate = useNavigate();
//   const { user } = useAuth();
//   const dispatch = useAppDispatch();

//   const handleLogOut = () => {
//     dispatch(quanLyNguoiDungActions.logOut());
//   };
//   return (
//     <Header className="">
//       <nav className=" px-4 lg:px-6  h-full flex items-center justify-center  ">
//         <div className="flex flex-wrap justify-between items-center w-full">
//           <div>
//             <div
//               className="flex items-center font-bold gap-2 text-2xl font-serif cursor-pointer"
//               onClick={() => navigate("/")}
//             >
//               <RiMovie2Fill />
//               <span className="text-black">Movie</span>
//             </div>
//           </div>
//           <div>
//             <ul className="flex mt-4 font-medium lg:flex-row lg:space-x-8 lg:mt-0">
//               <li className="">
//                 <a
//                   href="#lichChieu"
//                   className="block py-2 pr-4 pl-3 text-gray-700 rounded bg-primary-700 hover:text-red-500 transition duration-300 "
//                 >
//                   Lịch chiếu
//                 </a>
//               </li>
//               <li>
//                 <a
//                   href="#cumRap"
//                   className="block py-2 pr-4 pl-3 text-gray-700 rounded bg-primary-700 hover:text-red-500 transition duration-300"
//                 >
//                   Cụm rạp
//                 </a>
//               </li>
//               <li>
//                 <a
//                   href="#"
//                   className="block py-2 pr-4 pl-3 text-gray-700 rounded bg-primary-700 hover:text-red-500 transition duration-300 "
//                 >
//                   Tin tức
//                 </a>
//               </li>
//               <li>
//                 <a
//                   href="#"
//                   className="block py-2 pr-4 pl-3 text-gray-700 rounded bg-primary-700 hover:text-red-500 transition duration-300 "
//                 >
//                   Ứng dụng
//                 </a>
//               </li>
//             </ul>
//           </div>
//           {user ? (
//             <div className="flex gap-5">
//               <Popover
//                 content={
//                   <div>
//                     <h2 className="font-semibold p-2">{user?.taiKhoan}</h2>
//                     <hr />
//                     <div
//                       className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
//                       onClick={() => navigate(PATH.account)}
//                     >
//                       Thông tin tài khoản
//                     </div>
//                     <div
//                       className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
//                       onClick={handleLogOut}
//                     >
//                       Đăng xuất
//                     </div>
//                   </div>
//                 }
//                 trigger="click"
//               >
//                 <RxAvatar className="w-7 h-7" />
//               </Popover>
//               <div>
//                 {user.maLoaiNguoiDung.replace(/\s/g, "") === "QuanTri" && (
//                   <Popover
//                     content={
//                       <div>
//                         <h2 className="font-semibold p-2">Quản trị viên</h2>
//                         <hr />
//                         <div
//                           className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
//                           onClick={() => navigate(PATH.films)}
//                         >
//                           Đi tới trang quản lý
//                         </div>
//                       </div>
//                     }
//                   >
//                     <MdManageAccounts className="w-7 h-7" />
//                   </Popover>
//                 )}
//               </div>
//             </div>
//           ) : (
//             <div className="inline-flex">
//               <button
//                 className="bg-white text-gray-500 font-bold py-2 px-4 rounded-l  hover:text-red-500 transition duration-300 border-r "
//                 onClick={() => navigate("/login")}
//               >
//                 <div className="flex items-center gap-2 text-[17px] ">
//                   <RxAvatar className="w-7 h-7" />
//                   <span> Đăng nhập</span>
//                 </div>
//               </button>
//               <button
//                 className="bg-white  text-gray-500 font-bold py-2 px-4 rounded-r  hover:text-red-500 transition duration-300"
//                 onClick={() => navigate("/register")}
//               >
//                 <div className="flex items-center gap-2 text-[17px]">
//                   <RxAvatar className="w-7 h-7" />
//                   <span className=""> Đăng ký</span>
//                 </div>
//               </button>
//             </div>
//           )}
//         </div>
//       </nav>
//     </Header>
//   );
// };

// export default Navbar;

// const Header = styled.header`
//   height: var(--header-height);
//   width: var(--max-width);
//   margin: auto;
// `;
