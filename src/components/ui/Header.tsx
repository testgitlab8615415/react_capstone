import { NavLink, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Avatar, Popover } from ".";
import { UserOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { useAuth } from "hooks";
import { useAppDispatch } from "store";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
// import { useSelector } from 'react-redux'

export const Header = () => {
  const navigate = useNavigate();
  const { user } = useAuth();
  const dispatch = useAppDispatch();

  return (
    <HeaderS>
      <div className="header-content">
        <h2
          className="font-600 text-[30px] cursor-pointer"
          onClick={() => {
            navigate("/");
          }}
        >
          CYBER MOVIE
        </h2>
        <div className="flex items-center justify-around">
          <NavLink className="mr-40" to={PATH.about}>
            About
          </NavLink>
          <NavLink to={PATH.contact}>Contact</NavLink>
          {user && (
            <Popover
              content={
                <div className="p-10">
                  <h2
                    className="font-600 mb-10 p-10 cursor-pointer "
                    onClick={() => {
                      navigate("/");
                    }}
                  >
                    {user?.hoTen}
                  </h2>
                  <hr />
                  <div
                    className="!p-10 !mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300"
                    onClick={() => {
                      navigate(PATH.account);
                    }}
                  >
                    Thông tin tài khoản
                  </div>
                  <div
                    className="p-10 mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300"
                    onClick={() => {
                      dispatch(quanLyNguoiDungActions.logOut());
                    }}
                  >
                    Đăng xuất
                  </div>
                </div>
              }
              trigger="click"
            >
              <Avatar
                className="!ml-40 !cursor-pointer !flex !items-center !justify-center"
                size={30}
                icon={<UserOutlined />}
              />
            </Popover>
          )}
          {!user && (
            <p
              className="font-600 text-16 ml-40 cursor-pointer"
              onClick={() => {
                navigate(PATH.login);
              }}
            >
              Login
            </p>
          )}
        </div>
      </div>
    </HeaderS>
  );
};

export default Header;

const HeaderS = styled.header`
  height: var(--header-height);
  background: white;
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  .header-content {
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
  }
`;
