import { Pagination as Pa, PaginationProps as PaProps } from "antd";

export const Pagination = (props: PaProps) => {
  return <Pa {...props} />;
};

export default Pagination;
