import { Footer, Modal } from "components/ui";
import { PATH } from "constant";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate, useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import Youtube from "react-youtube";
import { getMovieDetailThunk } from "store/quanLyPhim/thunk";
import { getInfoLichChieuPhimThunk } from "store/quanLyRap/thunk";
import styled from "styled-components";
export const DetailMovieTemplate = () => {
  const params = useParams();
  const { movieId } = params;

  let path = "";

  const navigate = useNavigate();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const { movie } = useSelector((state: RootState) => state.quanLyPhim);
  const { maLichChieu } = useSelector((state: RootState) => state.quanLyRap);

  const dispatch = useAppDispatch();

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    dispatch(getMovieDetailThunk(movieId));
    dispatch(getInfoLichChieuPhimThunk(movieId));
  }, [dispatch, movieId]);
  const movieID = movie?.trailer;
  const trailer = movieID?.substring(30, 41);
  return (
    <Container>
      <div>
        <div className=" min-h-screen grid place-items-center font-mono bg-gray-900">
              <div className="w-[200px] h-[10px]">
                <img
                  src={movie?.hinhAnh}
                  alt="pic"
                  className=" w-full rounded-md  transform translate-y-4 border-4 border-gray-300 shadow-lg"
                />
              </div>

          <div className="flex-col text-gray-300">
            <p className="pt-4 text-2xl font-bold">{movie?.tenPhim}</p>
            <hr className="hr-text" data-content="" />
            <p className="hidden md:block px-4 my-4 text-sm text-left">
              <span>Mô tả: </span>
              {movie?.moTa}
            </p>

            <p className="flex text-md px-4 my-2">
              Rating: 9.0/10
              <span className="font-bold px-2">|</span>
              Mood: Dark
            </p>

                <div className="text-xs">
                  <button
                    type="button"
                    className="border border-gray-400 text-gray-400 rounded-md px-5 py-5 m-2 transition duration-500 ease select-none hover:bg-gray-900 focus:outline-none focus:shadow-outline"
                    onClick={showModal}
                  >
                    TRAILER
                  </button>

                  <button
                    type="button"
                    className="border border-gray-400 text-gray-400 rounded-md px-5 py-5 m-2 transition duration-500 ease select-none hover:bg-gray-900 focus:outline-none focus:shadow-outline"
                  >
                    IMDB
                  </button>

                  <button
                    type="button"
                    className="border border-gray-400 text-gray-400 rounded-md px-5 py-5 m-2 transition duration-500 ease select-none hover:bg-gray-900 focus:outline-none focus:shadow-outline"
                    onClick={() => {
                      path = generatePath(PATH.purchase, {
                        maLichChieu: maLichChieu,
                      });
                      navigate(path);
                    }}
                  >
                    Đặt vé
                  </button>
                </div>
              </div>
         
            <div className="flex justify-between items-center px-4 mb-4 w-full"></div>
          
        </div>
      </div>
      <Modal
        open={isModalOpen}
        footer
        onCancel={handleCancel}
        className="!w-[39%]"
      >
        <Youtube videoId={trailer}></Youtube>
      </Modal>
      <Footer />
    </Container>
  );
};

export default DetailMovieTemplate;
const Container = styled.div`
  body {
    text-align: center;
  }
  button:hover{
    background-color:white;
  }

  .hr-text {
    line-height: 1em;
    position: relative;
    outline: 0;
    border: 0;
    color: black;
    text-align: center;
    height: 1.5em;
    opacity: 0.5;
  }
  .hr-text:before {
    content: "";
    background: linear-gradient(to right, transparent, #818078, transparent);
    position: absolute;
    left: 0;
    top: 50%;
    width: 100%;
    height: 1px;
  }
  .hr-text:after {
    content: attr(data-content);
    position: relative;
    display: inline-block;
    color: black;
    padding: 0 0.5em;
    line-height: 1.5em;
    color: #818078;
    background-color: #fcfcfa;
  }
`;
//hello dfdfdfdf