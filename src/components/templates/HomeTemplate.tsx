import { Card, Pagination, Skeleton, Tabs } from "components/ui";
import { Carousel } from "components/ui/Carousel";
import { useEffect } from "react";
import { useAppDispatch } from "store";
import {
  getBannerThunk,
  getMovieListPageThunk,
  getMovieListThunk,
} from "store/quanLyPhim/thunk";
import { getHeThongRapThunk } from "store/quanLyRap/thunk";
import { CumRapTab } from "./rap/CumRapTab";
import { useAuth } from "hooks";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
import { quanLyPhimActions } from "store/quanLyPhim/slice";
import { generatePath } from "react-router-dom";
import { PATH } from "constant";
import { CardItem } from "./card";
import { Tabs as AntTabs } from "antd";

export const HomeTemplate = () => {
  const {
    banner,
    movieList,
    isFetchingMovieList,
    heThongRapList,
    currentPage,
  } = useAuth();
  const dispatch = useAppDispatch();
  let path = "";

  // dispatch action thunk call api
  const handlePageChange = (page: number) => {
    dispatch(quanLyPhimActions.setCurrentPage(page));
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleTabClick = (key: any) => {
    dispatch(quanLyNguoiDungActions.changeCinema(key));
  };

  useEffect(() => {
    dispatch(getMovieListThunk());
    dispatch(getBannerThunk());
    dispatch(getMovieListPageThunk(currentPage));
    dispatch(getHeThongRapThunk());
  }, [dispatch, currentPage]);

  // dispatch action thunk call api

  const { TabPane } = AntTabs;

  const currentMovies = movieList?.filter((movie) => movie.dangChieu);
  const upcomingMovies = movieList?.filter((movie) => movie.sapChieu);
  if (isFetchingMovieList) {
    return (
      <div className="grid grid-cols-4 gap-20">
        {[...Array(16)].map((_, index) => {
          return (
            <Card key={index} className="!w-[280px]">
              <Skeleton.Image active className="!w-full !h-[250px]" />
              <Skeleton.Input active className="!w-full !mt-10" />
              <Skeleton.Input active className="!w-full !mt-10" />
            </Card>
          );
        })}
      </div>
    );
  }

  return (
    <div>
      <Carousel className="text-black" autoplay>
        {banner?.map((banner) => {
          return (
            <div
              key={banner.maPhim}
              className="w-full mb-[50px] object-cover h-full"
            >
              <img
                className="w-[1400px] h-[500px]"
                src={banner.hinhAnh}
                alt=""
              />
            </div>
          );
        })}
      </Carousel>

      <div>
        <Tabs defaultActiveKey="dang-chieu" type="card" size="large">
          <TabPane tab="Đang Chiếu" key="dang-chieu">
            <div className="grid grid-cols-4 gap-30">
              {currentMovies?.map((movie) => {
                path = generatePath(PATH.moviedetail, {
                  movieId: movie.maPhim,
                });
                return (
                  <CardItem movie={movie} key={movie.maPhim} path={path} />
                );
              })}
            </div>
          </TabPane>
          <TabPane tab="Sắp Chiếu" key="sap-chieu">
            <div className="grid grid-cols-4 gap-30">
              {upcomingMovies?.map((movie) => {
                path = generatePath(PATH.moviedetail, {
                  movieId: movie.maPhim,
                });
                return (
                  <CardItem movie={movie} key={movie.maPhim} path={path} />
                );
              })}
            </div>
          </TabPane>
        </Tabs>
        <Pagination
          defaultCurrent={1}
          onChange={handlePageChange}
          total={40}
          className="!my-[50px] text-center"
        />
      </div>

      <div>
        <Tabs
          tabPosition="left"
          className="h-full w-full"
          tabBarGutter={-5}
          onTabClick={handleTabClick}
          items={heThongRapList?.map((rap) => {
            return {
              label: (
                <div className=" w-10 h-10 md:w-16 md:h-16">
                  <img className="w-[100%]" src={rap.logo} alt="" />
                </div>
              ),
              key: rap.maHeThongRap,
              children: <CumRapTab />,
            };
          })}
        />
      </div>
    </div>
  );
};

export default HomeTemplate;
