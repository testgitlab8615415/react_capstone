// import { Modal } from 'antd'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Input } from 'components/ui'
import { useAuth } from 'hooks'
import { useEffect } from 'react'
import { useForm ,SubmitHandler} from 'react-hook-form'
import { useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import { AccountSchema, AccountSchemaType } from 'schema'
import { RootState, useAppDispatch } from 'store'
import { updateUserThunk } from 'store/quanLyNguoiDung/thunk'
import { styled } from 'styled-components'

const AccountInfoTab = () => {
    const { user } = useAuth()
    const dispatch=useAppDispatch()
    const {isUpdatingUser}=useSelector((state:RootState)=>state.quanLyNguoiDung)

    const { reset, register,handleSubmit,formState:{errors} } = useForm<AccountSchemaType>({
        mode:'onChange',
        resolver:zodResolver(AccountSchema),

    })
    useEffect(() => {
        reset({
            ...user,
            soDt:user?.soDT
        })
    }, [user, reset])
    // hàm reset là để đưa thông tin lên ô input
    const onSubmit:SubmitHandler<AccountSchemaType>=(value)=>{
        dispatch(updateUserThunk(value)).unwrap().then(()=>{
            toast.success("successfully updated")
        })
    }

    return (
        <form className="text-white px-40 w-[600px] h-[600px]" onSubmit={handleSubmit(onSubmit)}>
            <InputS label="Tài khoản" name="taiKhoan" error={errors?.taiKhoan?.message} register={register} />
            <InputS label="Họ và tên" name="hoTen" error={errors?.hoTen?.message} register={register} />
            <InputS label="Số điện thoại" name="soDt" error={errors?.soDt?.message} register={register} />
            <InputS label="Mã nhóm" name="maNhom" register={register}  error={errors?.maNhom?.message}/>
            <InputS label="Email" name="email" register={register}  error={errors?.email?.message}/>
            <InputS  label="Mã loại  người dùng" name="maLoaiNguoiDung" register={register}  error={errors?.maLoaiNguoiDung?.message}/>
            <div className="text-right">
                <Button loading={isUpdatingUser} htmlType='submit' className="mt-[60px] w-[200px] !h-[50px]" type="primary">
                    Lưu thay đổi
                </Button>
            </div>
        </form>
    )
}

export default AccountInfoTab

//kế thừa thẻ cpn input 
const InputS = styled(Input)`
    margin-top: 20px;
    height:50px
    input {
        
        background-color: transparent !important;
        border: 1px solid black;
        color: black;
    }
`
