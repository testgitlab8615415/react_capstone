import { Card } from "components/ui";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Movie } from "types";

export const CardItem = ({ movie, path }: { movie: Movie; path: string }) => {
  const navigate = useNavigate();

  return (
    <Container>
      <div>
        <Card
          key={movie.maPhim}
          hoverable
          style={{ width: 240 }}
          cover={
            <img
              alt="example"
              src={movie.hinhAnh}
              onClick={() => navigate(path)}
            />
          }
        >
          <Card.Meta
            title={movie.tenPhim}
            // description={movie.ngayKhoiChieu}
            // description={movie.moTa.substring(0, 50)}
          />
        </Card>
      </div>
    </Container>
  );
};

export default Card;
const Container = styled.div``;
