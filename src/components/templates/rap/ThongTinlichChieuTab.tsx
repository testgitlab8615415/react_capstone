import { PATH } from "constant";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getLichChieuThunk } from "store/quanLyRap/thunk";
import { ListCumRap } from "types";

interface ThongTinlichChieuTabProps {
  cumRap: ListCumRap | null;
}

const ThongTinlichChieuTab = ({ cumRap }: ThongTinlichChieuTabProps) => {
  const dispatch = useAppDispatch();
  const { chooseCinema, chooseCumRap } = useSelector(
    (state: RootState) => state.quanLyNguoiDung
  );
  let path = "";

  useEffect(() => {
    dispatch(getLichChieuThunk(chooseCinema));
    console.log("cinema", chooseCinema);
  }, [chooseCinema, chooseCumRap, dispatch]);

  
  const navigate = useNavigate();
  return (
    <div className="overflow-y-scroll h-[500px] pb-10 ">
      {cumRap?.danhSachPhim.map((dsPhim) => (
        <div className=" container mx-auto py-10">
          <div className="flex justify-center mx-auto my-10">
            <div className="flex flex-row rounded-lg bg-gray-100 shadow-md shadow-white">
              <div>
                <img
                  className="w-44 h-52 rounded-t-lg md:rounded-none md:rounded-l-lg"
                  src={dsPhim.hinhAnh}
                  alt=""
                />
              </div>
              <div className="flex flex-col  space-y-4">
                <div className="flex justify-between flex-col items-start w-[450px]">
                  <h2 className="text-[30px] font-bold w-full">
                    {dsPhim.tenPhim}
                  </h2>
                  <div className="flex mt-3 ">
                    <span className="font-bold ml-3 mt-2 text-[20px]">
                      Lịch chiếu:
                    </span>
                    <div className="ml-auto">
                      {dsPhim.lstLichChieuTheoPhim?.map(
                        // eslint-disable-next-line @typescript-eslint/no-explicit-any
                        (item: any, index: number) => {
                          if (index < 3)
                            return (
                              <div key={index}>
                                <button
                                  type="button"
                                  className=" font-semibold m-1 p-2 md:ml-3 md:p-3 duration-300 hover:bg-red-700 rounded bg-red-500 text-white"
                                  onClick={() => {
                                    path = generatePath(PATH.purchase, {
                                      maLichChieu: item.maLichChieu,
                                    });
                                    navigate(path);
                                  }}
                                >
                                  <div
                                    className=" flex gap-2 text-white "
                                    onClick={() => {
                                      navigate(PATH.moviedetail);
                                    }}
                                  >
                                    <span className="text-white">
                                      Ngày chiếu:
                                    </span>
                                    <p className="mr-3">
                                      {item.ngayChieuGioChieu.substr(0, 10)}
                                    </p>{" "}
                                    |<span className="ml-3 ">Giờ chiếu:</span>
                                    <p>
                                      {item.ngayChieuGioChieu.substr(11, 10)}
                                    </p>
                                  </div>
                                </button>
                              </div>
                            );
                          else return <></>;
                        }
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ThongTinlichChieuTab;



// import { PATH } from "constant";
// import { useEffect } from "react";
// import { useSelector } from "react-redux";
// import { generatePath, useNavigate } from "react-router-dom";
// import { RootState, useAppDispatch } from "store";
// import { getLichChieuThunk } from "store/quanLyRap/thunk";

// const ThongTinlichChieuTab = () => {
//   const dispatch =useAppDispatch();
//   const {chooseCinema,chooseCumRap  } = useSelector( (state: RootState) => state.quanLyNguoiDung
//   );
//   let path = "";
//   const { movies } = useSelector(
//     (state: RootState) => state.quanLyRap
//     );
//     const moviez=movies[0].lstCumRap;
//     console.log("zzzz",moviez)
//     // console.log("hello",moviez[chooseCumRap],chooseCumRap)
//     const moviezd=moviez.map((movie)=>movie.maCumRap);
//     console.log( "ss",moviezd[1])

//     useEffect(() => {
//       dispatch(getLichChieuThunk(chooseCinema))
//     }, [chooseCinema]);
//     console.log("cin",chooseCinema)

//     useEffect(()=>{

//     })
//   const navigate = useNavigate();
//   return (
//     <div className="overflow-y-scroll h-[500px] pb-10 ">
//       {/* eslint-disable-next-line @typescript-eslint/no-explicit-any */}
//       {moviez.map((movie) => {
        
//         if(movie.maCumRap){
        
//         return  movie.danhSachPhim.map((dsPhim)=>(
//   <div className=" container mx-auto py-10">
//           <div className="flex justify-center mx-auto my-10">
//             <div className="flex flex-row rounded-lg bg-gray-100 shadow-md shadow-white">
//               <div>
//                 <img
//                   className="w-44 h-52 rounded-t-lg md:rounded-none md:rounded-l-lg"
//                   src={dsPhim.hinhAnh}
//                   alt=''
//                 />
//               </div>
//               <div className="flex flex-col  space-y-4">
//                 <div className="flex justify-between flex-col items-start w-[450px]">
//                   <h2 className="text-[30px] font-bold w-full">
//                     {dsPhim.tenPhim}
//                   </h2>
//                   <div className="flex mt-3 " >
//                     <span className="font-bold ml-3 mt-2 text-[20px]">Lịch chiếu:</span>
//                     <div className="ml-auto">
//                       {dsPhim.lstLichChieuTheoPhim?.map(
//                         // eslint-disable-next-line @typescript-eslint/no-explicit-any
//                         (item: any, index: number) => {
//                           if (index < 3)
//                             return (
//                               <div key={index}>
//                                 <button
//                                   type="button"
//                                   className=" font-semibold m-1 p-2 md:ml-3 md:p-3 duration-300 hover:bg-red-700 rounded bg-red-500 text-white"
//                                   onClick={() => {
//                                     path = generatePath(PATH.purchase, {
//                                       maLichChieu: item.maLichChieu,
//                                     });
//                                     navigate(path);
//                                   }}
//                                 >
//                                   <div
//                                     className=" flex gap-2 text-white "
//                                     onClick={() => {
//                                       navigate(PATH.moviedetail);
//                                     }}
//                                   >
//                                     <span className="text-white">
//                                       Ngày chiếu:
//                                     </span>
//                                     <p className="mr-3">
//                                       {item.ngayChieuGioChieu.substr(0, 10)}
//                                     </p>{" "}
//                                     |<span className="ml-3 ">Giờ chiếu:</span>
//                                     <p>
//                                       {item.ngayChieuGioChieu.substr(11, 10)}
//                                     </p>
//                                   </div>
//                                 </button>
//                               </div>
//                             );
//                           else return;
//                         }
//                       )}
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//         ))}
//                       }
//                       )}
//     </div>
//   );
// };

// export default ThongTinlichChieuTab;
