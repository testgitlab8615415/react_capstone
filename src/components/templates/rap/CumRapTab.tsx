import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { getCumRapThunk, getLichChieuThunk } from "store/quanLyRap/thunk";
import ThongTinlichChieuTab from "./ThongTinlichChieuTab";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
import { ListCumRap } from "types";

export const CumRapTab = () => {
  const dispatch = useAppDispatch();
  const [rapDuocChon, setRapDuocChon] = useState<null | ListCumRap>(null);
  const { cumRap, movies } = useSelector((state: RootState) => state.quanLyRap);
  const { chooseCinema, chooseCumRap } = useSelector(
    (state: RootState) => state.quanLyNguoiDung
  );
  const handleTabClick = (key: string) => {
    dispatch(quanLyNguoiDungActions.changeCumRap(key));
  };
  useEffect(() => {
    dispatch(getCumRapThunk(chooseCinema));
    dispatch(getLichChieuThunk(chooseCinema));
  }, [chooseCinema, dispatch]);
 
  useEffect(() => {
    if (!movies?.[0]?.lstCumRap) return;
    const newListMovie = movies[0].lstCumRap as unknown as ListCumRap[];
    const index = newListMovie.findIndex(
      (item) => item.maCumRap === chooseCumRap
    );
    if (index !== -1) {
      setRapDuocChon(newListMovie[index]);
    }
  }, [movies, chooseCumRap]);
  useEffect(() => {
    if (!cumRap) return;
    dispatch(quanLyNguoiDungActions.changeCumRap(cumRap[0]?.maCumRap));
  }, [cumRap, dispatch]);


  return (
    <div className="flex flex-wrap">
      <div className="w-[300px]">
        {cumRap.map((rap) => {
          return (
            <div
              onClick={() => {
                handleTabClick(rap.maCumRap);
              }}
              className={`w-72 text-start flex flex-col px-1 rounded-md p-4 cursor-pointer ${
                rapDuocChon?.maCumRap === rap.maCumRap
                  ? "bg-slate-300"
                  : "bg-white"
              }`}
              key={rap.maCumRap}
            >
              <p className="text-xl font-semibold">{rap.tenCumRap}</p>
              <p className="break-all ">{rap.diaChi}</p>
            </div>
          );
        })}
      </div>

      <div className="w-[calc(100%_-_300px)]">
        <ThongTinlichChieuTab cumRap={rapDuocChon} />
      </div>
    </div>
  );
};

export default CumRapTab;
