export type QuanLyHeThongRap = {
  maHeThongRap: string;
  tenHeThongRap: string;
  biDanh: string;
  logo: string;
};
export type movie = [maRap: number, tenRap: string];
export type CumRap = {
  maCumRap: string;
  tenCumRap: string;
  diaChi: string;
  danhSachRap: movie[];
};

// export type PhimTheoLich=[
// danhSachPhim:[
//     lstLichChieuTheoPhim:[
//         maLichChieu: number,
//         maRap: number,
//      tenRap: string,
//         ngayChieuGioChieu:string,
//          giaVe: number

// ],
//     maPhim:  number,
//     tenPhim: string,
//     hinhAnh: string,
//     hot:  boolean,
//     dangChieu:  boolean,
//     sapChieu:  boolean,
// ]
// ]

// export type LichChieu={

// lstCumRap:PhimTheoLich[]

// }

export interface LstLichChieuTheoPhim {
  maLichChieu: number | string;
  maRap: string;
  tenRap: string;
  ngayChieuGioChieu: string;
  giaVe: number;
}

export interface DanhSachPhim {
  lstLichChieuTheoPhim: LstLichChieuTheoPhim[];
  maPhim: number | string;
  tenPhim: string;
  hinhAnh: string;
  hot?: boolean;
  dangChieu?: boolean;
  sapChieu?: boolean;
}
export interface ListCumRap {
  danhSachPhim: DanhSachPhim[];
  maCumRap: string;
  tenCumRap: string;
  hinhAnh: string;
  diaChi: string;
}

export type LichChieu = {
  lstCumRap: {
    danhSachPhim: {
      lstLichChieuTheoPhim: {
        maLichChieu: number;
        maRap: string;
        tenRap: string;
        ngayChieuGioChieu: string;
        giaVe: number;
      }[];
      maPhim: number;
      tenPhim: string;
      hinhAnh: string;
      hot: boolean;
      dangChieu: boolean;
      sapChieu: boolean;
    }[];
    maCumRap: string;
    tenCumRap: string;
    hinhAnh: string;
    diaChi: string;
  }[];
  maHeThongRap: string;
  tenHeThongRap: string;
  logo: string;
  mahom: string;
};
[];
